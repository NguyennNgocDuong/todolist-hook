import logo from './logo.svg';
import './App.css';
import TodoList from './BaiTapStyleComponent/TodoList/TodoList';

function App() {
  return (
    <div className="App">
      <TodoList />
    </div>
  );
}

export default App;
