import { arrTheme } from "../../Themes/ThemeManager"
import { ToDoListLightTheme } from "../../Themes/ToDoListLightTheme"
import { add_task, change_theme } from "../constants/TodoListConstant"

const initialState = {
    themeToDoList: ToDoListLightTheme,
    taskList: [
        { id: "task-1", taskName: 'task 1', done: true },
        { id: "task-2", taskName: 'task 2', done: false },
        { id: "task-3", taskName: 'task 3', done: true },
    ]
}

export const todoListReducer = (state = initialState, { type, newTask, valueTheme }) => {
    switch (type) {

        case add_task: {
            // kiem tra rỗng
            if (newTask.taskName.trim() === "") {
                alert('Task name is required!!!')
                return { ...state }
            }

            // kiem tra trung
            let newTaskList = [...state.taskList]

            let index = newTaskList.findIndex((task) => task.taskName === newTask.taskName)
            if (index !== -1) {
                alert('Task name is already exists!!!')
                return { ...state }
            }

            state.taskList = [...newTaskList, newTask]
            return { ...state }
        }

        case change_theme: {
            let theme = arrTheme.find(theme => theme.id == valueTheme)
            if (theme) {
                console.log('theme: ', theme);
                state.themeToDoList = { ...theme.theme }
            }
            return { ...state }
        }

        default:
            return state
    }
}
