import { combineReducers } from "redux";
import { todoListReducer } from "./TodoListReducer";

export const rootReducer = combineReducers({
    todoListReducer
})