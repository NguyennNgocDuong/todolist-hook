import { add_task, change_theme } from "../constants/TodoListConstant";

export const addTaskAction = (newTask) => ({
    type: add_task,
    newTask
})

export const changeThemeAction = (valueTheme) => ({
    type: change_theme,
    valueTheme
})
