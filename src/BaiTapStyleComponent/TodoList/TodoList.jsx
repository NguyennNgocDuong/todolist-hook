import { ThemeProvider } from "styled-components";
import { Button } from "../../Components/Button";
import { Container } from "../../Components/Container";
import { Dropdown } from "../../Components/Dropdown";
import { Heading1, Heading3 } from "../../Components/Heading";
import { Table, Th, Thead, Tr } from "../../Components/Table";
import { Input, Label, TextField } from "../../Components/TextField";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Themes/ToDoListPrimaryTheme";

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
} from "../../redux/actions/TodoListAction";
import { arrTheme } from "../../Themes/ThemeManager";

class TodoList extends Component {
  state = {
    taskName: "",
  };
  renderTheme = () => {
    return arrTheme.map((theme) => (
      <option value={theme.id}>{theme.name}</option>
    ));
  };
  changeTheme = (e) => {
    let { value } = e.target;
    this.props.dispatch(changeThemeAction(value));
  };
  addTask = () => {
    // lấy thông tin người dùng nhập từ input
    let { taskName } = this.state;

    // tạo ra obj task mới
    let newTask = {
      id: Date.now(),
      taskName,
      done: false,
    };
    // đưa obj task lên redux thông qua đispatch
    this.props.dispatch(addTaskAction(newTask));
  };

  renderTaskTodo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task) => {
        return (
          <Tr key={task.id}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-2">
                <i class="fa fa-edit"></i>
              </Button>
              <Button className="ml-2">
                <i class="fa fa-check-circle"></i>
              </Button>
              <Button className="ml-2">
                <i class="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskDone = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task) => {
        return (
          <Tr key={task.id}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-2">
                <i class="fa fa-edit"></i>
              </Button>
              <Button className="ml-2">
                <i class="fa fa-check-circle"></i>
              </Button>
              <Button className="ml-2">
                <i class="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown onChange={this.changeTheme}>{this.renderTheme()}</Dropdown>
          <Heading1>To Do List</Heading1>
          <TextField
            onChange={(e) => this.setState({ taskName: e.target.value })}
            name="taskName"
            label="Task name"
            className="w-50"
          />
          <Button onClick={this.addTask} className="ml-2">
            <i class="fa fa-plus-circle"></i> Add task
          </Button>
          <Button className="ml-2">
            <i class="fa fa-upload"></i> Update task
          </Button>
          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskTodo()}</Thead>
          </Table>
          <hr />
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskDone()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.todoListReducer.themeToDoList,
    taskList: state.todoListReducer.taskList,
  };
};

export default connect(mapStateToProps)(TodoList);
